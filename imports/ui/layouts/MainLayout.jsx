import React from 'react';

export const MainLayout = ({ content }) => (
    /** 
     * plantilla layout encargada de insertar el contenido de react
    */
      <div className="main-layout">      
        <div className="feature-section container">
            {content}
        </div>
    </div>
)
