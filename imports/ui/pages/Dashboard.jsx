import { Meteor } from 'meteor/meteor'
import React, { Component } from 'react';

import '../StyleApp.css'
import './StyleDashboard.css'

export default class Dashboard extends Component {
    /** 
     * Componente encargado de manejar toda la logica de la aplicacion de el lado de el cliente
    */

    constructor(props) {
        super(props);
        this.state = {
          user : '',
          tasks: [],
          all: [],
          all_users: [],
          process: 'create',
          edit_id: ''
        };

        this.getUserName()
        this.getTasks()

        setTimeout(() => {
            this.setState({user : Meteor.user().profile.name})        
            if (Roles.userIsInRole(Meteor.userId(), ['superuser'])) {
                this.getTasksAll()
                this.getUsersAll()
            }
        }, 1000);
    }

    getUserName = () => {
        /** 
         * funcion encargada de obtener la reactividad de los usuarios
        */

        setTimeout(() => {
            this.setState({user : Meteor.user().profile.name})        
        }, 1000);
    }

    getTasks = () => {
        /** 
         * funcion encargada de obtener la reactividad de los las tareas
        */

        Meteor.call('api/get/task', Meteor.userId(), (err, response) => {
            this.setState({tasks: response})
        })
    }

    getTasksAll = () => {
        /** 
         * funcion encargada de obtener la reactividad de los las tareas de todos los usuarios solo para el administrador
        */

        Meteor.call('api/getall/task', (err, response) => {
            this.setState({all: response})
        })
    }
    
    getUsersAll = () => {
        /** 
         * funcion encargada de obtener la reactividad de todos los usuarios solo para el administrador
        */
        Meteor.call('api/getall/users', (err, response) => {
            this.setState({all_users: response})
        })
    }
    

    HandlerCreateTask = (event) => {
        /** 
         * funcion encargada de crear la tarea editar y disparar reactivamente las tareas
        */
        event.preventDefault()

        let data = {
            name: document.getElementById('nameTask').value,
            description: document.getElementById('descriptionTask').value,
            date: document.getElementById('dateTask').value
        }

        if (this.state.process == 'create'){
            Meteor.call('api/create/task', data, (err, response) => {
                if (!err){
                    Bert.alert( 'Se ha creado correctamente' , 'success');
                    this.getTasks()
                    this.CleanFormTask()
                }
            })
        }else if (this.state.process == 'edit'){
            Meteor.call('api/update/task', this.state.edit_id, data, (err, response) => {
                if (!err){
                    Bert.alert( 'Se ha creado correctamente' , 'success');
                    this.getTasks()
                    this.setState({
                        process: 'create',
                        edit_id: ''
                    })
                    this.CleanFormTask()
                    if (Roles.userIsInRole(Meteor.userId(), ['superuser'])) {
                        this.getTasksAll()
                    }
                }
            })
        }

        this.HandlerCreateNew()
    }

    CleanFormTask = () => {
        /** 
         * funcion encargada de limpiar el formulario
        */
        document.getElementById('nameTask').value = ""
        document.getElementById('nameTask').value = ""
        document.getElementById('dateTask').value = ""
    }

    HandlerClickUpdate = (event) => {
        /** 
         * Actializar las tareas
        */

        event.preventDefault()

        this.setState({
            process: 'edit',
            edit_id: event.currentTarget.dataset.id
        })

        let data = JSON.parse(event.currentTarget.dataset.data)

        document.getElementById('nameTask').value = data.name
        document.getElementById('descriptionTask').value = data.description
        document.getElementById('dateTask').value = data.date

        document.querySelector('.createTask').style.display = ''
    }

    HandlerClickDelete = (event) => {
        /** 
         * eliminar las tareas
        */

        Meteor.call('api/delete/task', event.currentTarget.dataset.id, (err, response) => {
            if (!err){
                Bert.alert( 'Se ha eliminado correctamente' , 'success');
                this.getTasks()
                if (Roles.userIsInRole(Meteor.userId(), ['superuser'])) {
                    this.getTasksAll()
                }        
            }
        })
    }

    HandlerCreateNew = (event) => {
        /** 
         * evento que muestra el formulario para crear las tareas
        */

        if (document.querySelector('.createTask').style.display == 'none'){
            document.querySelector('.createTask').style.display = ''
            this.CleanFormTask()
        }else{
            document.querySelector('.createTask').style.display = 'none'
            this.CleanFormTask()
        }
    }

    HandlerClickLogOut = () => {
        /** 
         * cerrar la session
        */
        Meteor.logout()
        FlowRouter.go('App');
    }

    HandlerShowClients = () => {
        /** 
         * muestra la lista de clientes y la remplaza por la lista de tareas
        */

        document.querySelector('#tasks_list').style.display = "none"
        document.querySelector('#users_list').style.display = ""
    }

    HandlerClickDeleteClient = (event) => {
        let uid = event.currentTarget.dataset.id
        swal({
            title: 'Estas seguro que deseas eliminar?',
            text: 'Una vez verificado no tendra vuelta atras!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, Eliminalo!',
            cancelButtonText: 'No, Dejalo',
          }).then(function() {
              Meteor.call('api/delete/users', uid, (err, response) => {
                if (!err){
                    swal(
                    'Eliminado correctamente!',
                    'Your imaginary file has been deleted.',
                    'success'
                    );
                    this.getUsersAll()
                } 
              })
          }, function(dismiss) {
            // dismiss can be 'cancel', 'overlay', 'close', 'timer'
            if (dismiss === 'cancel') {
              swal(
                'Cancelado',
                'Ninguna acción realizada',
                'error'
              );
            }
          });
    }

    HandlerCreateNewUser = () => {
        /** 
         * evento para limpiar el formulario de crear usuario y de mostrarlo o ocultarlo
        */
        if (document.querySelector('.createUser').style.display == 'none'){
            document.querySelector('.createUser').style.display = ''
            this.CleanFormTask()
        }else{
            document.querySelector('.createUser').style.display = 'none'
            this.CleanFormTask()
        }

        document.getElementById('userName').value = ""
        document.getElementById('emilUser').value = ""
        document.getElementById('passwordUser').value = ""
    }

    HandlerClickCreateUser = () => {
        /** 
         * evento encargado de crear un nuevo usuario por consultas en la base de datos
        */
        if (document.getElementById('userName').value != "" &&
            document.getElementById('emilUser').value != "" &&
            document.getElementById('passwordUser').value != ""){
            let data = {
                name: document.getElementById('userName').value,
                email: document.getElementById('emilUser').value,
                pass: document.getElementById('passwordUser').value
            }

            Meteor.call('api/create/user', data, (err, response) => {
                if (!err){

                    Bert.alert( 'Se ha creado correctamente' , 'success');
                    this.getTasks()
                    this.CleanFormTask()
                    this.HandlerCreateNewUser()
                    this.getUsersAll()
                }
            })
        }else{
            Bert.alert( 'Todos los campos para crear el usuario son necesarios' , 'warning');
        }
    }

    render() {
        return (
            <div >
                <nav className='float_menu'>
                    <p>Usuario : {this.state.user}</p>
                    {
                    Roles.userIsInRole(Meteor.userId(), ['superuser'])?
                        <button style={{background: '#ececec'}} onClick={this.HandlerShowClients} className="btn">Usuarios</button>
                        
                    :''
                    }
                    <button onClick={this.HandlerClickLogOut} className="btn" style={{ maxWidth: '125px'}}>Cerrar Sessión</button>
                </nav>

                <div className="strix" id="tasks_list">
                    <div className="list_tasks">
                        <button className="btn" onClick={this.HandlerCreateNew}>Crear Nueva</button>

                        <div className="createTask" style={{display:'none'}}>
                            <div>
                                <label htmlFor="nameTask">Nombre Tarea: </label>
                                <input type="text" id="nameTask"/>
                            </div>
                            <div>
                                <label htmlFor="descriptionTask">Descripcion: </label>
                                <textarea type="textarea" id="descriptionTask"/>
                            </div>
                            <div>
                                <label htmlFor="dateTask">Fecha: </label>
                                <input type="date" id="dateTask"/>
                            </div>

                            <button className="btn" onClick={this.HandlerCreateTask}>{this.state.process == 'create'?'Crear':'Editar'}</button>
                        </div>


                        <div>
                            <h3>Mis Tareas</h3>
                            <div>
                                {
                                    this.state.tasks.length > 0?
                                    this.state.tasks.map((item, key) => (
                                        <div key={item._id} className="item_list">
                                            <p>Nombre: {item.name} Fecha: {item.date}</p>
                                            <p>{item.description}</p>
                                            <button style={{background: '#5ad45a'}} className="btn" data-id={item._id} onClick={this.HandlerClickUpdate} data-data={JSON.stringify(item)} >Update</button>
                                            <button style={{background: '#ff5959', marginLeft: '10px'}} className="btn" data-id={item._id} onClick={this.HandlerClickDelete}>Delete</button>
                                        </div>
                                    ))
                                    :
                                    <div><strong>Aún no hay tareas</strong></div>
                                }
                            </div>
                        </div>

                        {
                            <div>
                                <h3>Todas las tareas de los usuarios</h3>
                                <div>
                                {
                                    Roles.userIsInRole(Meteor.userId(), ['superuser'])?
                                        this.state.all.length > 0?
                                        this.state.all.map((item, key) => (
                                            <div key={item._id} className="item_list">
                                                <p>Nombre: {item.name} Fecha: {item.date}</p>
                                                <p>{item.description}</p>
                                                <button style={{background: '#5ad45a'}} className="btn" data-id={item._id} onClick={this.HandlerClickUpdate} data-data={JSON.stringify(item)} >Update</button>
                                                <button style={{background: '#ff5959', marginLeft: '10px'}} className="btn" data-id={item._id} onClick={this.HandlerClickDelete}>Delete</button>
                                            </div>
                                        ))
                                        :<div><strong>Aún no hay tareas</strong></div>
                                    :''
                                    }
                                </div>
                            </div>

                        }
                    </div>
                </div>

                <div className="strix" id="users_list" style={{display: 'none'}}>

                <div>
                    <h3>Lista de usuarios</h3>
                    
                    <div>
                        <button className="btn" onClick={this.HandlerCreateNewUser}>Crear Nuevo Usuario</button>

                        <div className="createUser" style={{display:'none'}}>
                            <div>
                                <label htmlFor="userName">Nombre de Usuario: </label>
                                <input type="text" id="userName"/>
                            </div>
                            <div>
                                <label htmlFor="emilUser">Email: </label>
                                <input type="email" id="emilUser"/>
                            </div>
                            <div>
                                <label htmlFor="passwordUser">Contraseña: </label>
                                <input type="password" id="passwordUser"/>
                            </div>

                            <button className="btn" onClick={this.HandlerClickCreateUser}>Crear</button>
                        </div>
                    </div>


                    <div>
                    {
                        Roles.userIsInRole(Meteor.userId(), ['superuser'])?
                            this.state.all_users.length > 0?
                            this.state.all_users.map((item, key) => (
                                <div key={item._id} className="item_list">
                                    <p>Nombre: {item.profile.name}</p>
                                    <p>Email: {item.emails[0].address}</p>
                                    <button style={{background: '#ff5959'}} className="btn" data-id={item._id} onClick={this.HandlerClickDeleteClient}>Delete</button>
                                </div>
                            ))
                            :<div><strong>Aún no hay usuarios</strong></div>
                        :''
                        }
                    </div>
                </div>
                </div>
            </div>
        )
    }
}