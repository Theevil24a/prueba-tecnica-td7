import React, { Component } from 'react';

import './StyleApp.css'

export default class App extends Component {
    /**  
     * componente de logim
    */
    HandlerSubmitLogin = (event) => {
        /** 
         * funcion encargada de enviar el formulario de login
        */

        let ready = false;

        let email = document.getElementById('email').value,
        password = document.getElementById('password').value;

        if(email != '' ){
            ready = true;
        }

        if( password != '' ){
            ready = true
        }

        Meteor.loginWithPassword(email, password, function (err, res) {
            if (err) {
              
                if (err.message === 'User not found [403]') {


                    Bert.alert( 'User not found [403]' , 'danger');

                } else {

                    Bert.alert( 'password wrong' , 'danger');

                }
                
            } else {
                FlowRouter.go('App.home');

            }
        });
    }

    render() {
        return (
            <div>
                <div id="login" className="strix">
                    <h1>Iniciar session</h1>

                    <div>
                        <label htmlFor="email">Correo: </label>
                        <input type="email" id="email"/>
                    </div>
                    <div>
                        <label htmlFor="password">Contraseña: </label>
                        <input type="password" id="password"/>
                    </div>

                    <button className="btn" onClick={this.HandlerSubmitLogin}>Iniciar</button>
                </div>
            </div>
        )
    }
}


