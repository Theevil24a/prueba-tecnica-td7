import { Mongo } from 'meteor/mongo';

/** 
 * colleccion en la base de datos mongo
*/
export const Task = new Meteor.Collection('tasks');
