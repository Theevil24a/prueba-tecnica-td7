// Tests for links methods
//
// https://guide.meteor.com/testing.html

import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { Task } from './task';
import './methods.js';

if (Meteor.isServer) {
	
	describe('tasks methods', function () {
		/** 
		 * Prueva unitaria de el metodo para crear
		*/
		beforeEach(function () {
			Task.remove({});
		});

		it('can add a new Tasks', function () {
			
			const addMovie = Meteor.server.method_handlers['api/create/task'];

			const data = {
				name: '',
                description: '',
                date: '',
                createdAt: '',
                user: ''
			}
	

			addMovie.apply({}, [ data ] );
			assert.equal(Task.find().count(), 1);

		});
	
    });
}