import { Meteor } from 'meteor/meteor';

import { Task } from '../task/task'

let schema = (data, add) => {
    /** 
     *  schema de diseño de la colleccion tasks
     * 
    */
    let sc = {
        name: data.name,
        description: data.description,
        date: data.date,
    }

    if (add) {
        sc['createdAt'] = new Date()
        sc['user'] = Meteor.userId()
    }

    return sc
}

Meteor.methods({
    'api/create/task'(data){
        /** 
         * api para insertar tarea
        */
        return Task.insert(schema(data, true))
    },

    'api/delete/task'(id){
        /** 
         *  api para eliminar tarea
        */
        return Task.remove({_id: id})
    },

    'api/update/task'(id, data){
        /** 
         * api para actualizar tarea
        */
        if (!Roles.userIsInRole(Meteor.userId(), ['superuser'])){
            data['user'] = Meteor.userId()
            data['editAt'] = new Date()
        }else{
            let task = Task.findOne({_id: id})

            data['user'] = task.user
        }
        return Task.update({_id: id}, {$set: schema(data, false)})
    },

    'api/get/task'(userId){
        /** 
         * api para obtener tarea
        */
        return Task.find({user: userId}).fetch()
    },

    'api/getall/task'(){
        /** 
         * api para obtener todas las tareas solo si tiene el rol administrador
        */
        if (Roles.userIsInRole(Meteor.userId(), ['superuser'])){
            return Task.find().fetch()
        }
    },

    'api/getall/users'(){
        /** 
         * api para obtener toda la lista de usuarios solo si es el administrador
        */
        if (Roles.userIsInRole(Meteor.userId(), ['superuser'])){
            return Meteor.users.find({roles: 'standard'}).fetch()
        }
    },

    'api/delete/users'(uid){
        /** 
         * api para eliminar un usuario de la lista de usuario 
        */
        if (Roles.userIsInRole(Meteor.userId(), ['superuser'])){
            return Meteor.users.remove({_id: uid})
        }
    },

    'api/create/user'(data){
        /** 
         * api para crear un nuevo usuario 
        */

        if (Roles.userIsInRole(Meteor.userId(), ['superuser'])){
            data['roles'] = ['standard']

            var users = [data];
    
            for (user of users) {
    
                id = Accounts.createUser({
                    email: user.email,
                    password: user.pass,
                    profile: {
                        name: user.name,
                        status: true,
                    }
                });
    
                if (user.roles.length > 0) {
                    Roles.addUsersToRoles(id, user.roles);
                }
    
            }
        }
    }
    

});