import { FlowRouter } from 'meteor/kadira:flow-router';
import React from 'react';
import {mount} from 'react-mounter';

import { MainLayout } from '../../ui/layouts/MainLayout.jsx'
import App from '../../ui/App';
import Dashboard from '../../ui/pages/Dashboard';

/** 
 * archivo de manejo de rutas
*/


FlowRouter.route('/',{
    /** 
     * ruta principal de login
    */
    name:'App',
	action(){
        mount(MainLayout,{
            content: (<App />)
        });
	}
});


FlowRouter.route('/dash',{
    /** 
     * runta de el dashboard
    */
    name:'App.home',
	action(){
        //react mounter
        mount(MainLayout,{
            content: (<Dashboard />)
        });
	}
});
