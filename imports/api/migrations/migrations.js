import { Mongo } from 'meteor/mongo';

/** 
 * colleccion para las migraciones de los fixes que es el archivo en la carpeta startup 
 * 
*/
export const Migrations = new Meteor.Collection('migrations');
