// Tests for the behavior of the links collection
//
// https://guide.meteor.com/testing.html

import { Meteor } from 'meteor/meteor';
import { assert } from 'chai';
import { Task } from './task';

if ( Meteor.isServer ) {
	
	describe('tasks collection', function () {
		/** 
		 * pruevas unitarias de la colleccion
		*/
		it('insert tasks correctly', function () {
			
			const movieId = Movies.insert({
                    name: '',
                    description: '',
                    date: '',
                    createdAt: '',
                    user: ''
			});

			const added = Task.find({ _id: movieId });
			const collectionName = added._getCollectionName();
			const count = added.count();

			assert.equal(collectionName, 'tasks');
			assert.equal(count, 1);
			
		});

	});

}