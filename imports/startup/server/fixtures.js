import { Meteor } from 'meteor/meteor'
import { Migrations } from '../../api/migrations/migrations';
import { Roles } from 'meteor/alanning:roles';

Meteor.startup(() => {
	/** 
	 * funcion encargada de realizar las inserciones iniciales a la base de datos
	*/

	if (!Migrations.findOne({ slug: 'users' })) {
		var users = [
			{ name: "Admin", email: "admin@user.com", pass: "123", roles: ['superuser'] },
			{ name: "User", email: "user@user.com", pass: "123", roles: ['standard'] },
		];

		for (user of users) {

			id = Accounts.createUser({
				email: user.email,
				password: user.pass,
				profile: {
					name: user.name,
					status: true,
				}
			});

			if (user.roles.length > 0) {
				Roles.addUsersToRoles(id, user.roles);
			}

		}

		Migrations.insert({ slug: 'users' });
		console.log('Running migrations for users...');
	}
});